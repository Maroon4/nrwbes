const mongoose = require('./libs/mongoose');
mongoose.set('debug', true);
const async = require('async');


async.series([
    open,
    dropDB,
    requireModels,
    setUsers
], function (err, results) {
    console.log(arguments);
    mongoose.disconnect();
    process.exit(err ? 255 : 0);
});

function open(callback) {
    mongoose.connection.on('open', callback);
}

function dropDB(callback) {
    const db = mongoose.connection.db;
    db.dropDatabase(callback);
}

function requireModels(callback) {
    require('./modeles/user').User;

    async.each(Object.keys(mongoose.models), function (modelName, callback) {
        mongoose.models[modelName].ensureIndexes(callback);
        // mongoose.models[modelName].createIndexes(callback);
    }, callback);
}

function setUsers(callback) {

    const users = [
        {username: 'Вася', password: 'supervasya'},
        {username: 'Петя', password: '123'},
        {username: 'admin', password: 'thetruehero'}
    ];

    async.each(users, function (userData, callback) {
        const user = new mongoose.models.User(userData);
        user.save(callback)
    }, callback);

    // async.parallel([
    //     function (callback) {
    //         const vasya = new User( {username: 'Вася', password: 'supervasya'},);
    //         vasya.save(function (err) {
    //             callback(err, vasya);
    //         })
    //     },
    //     function (callback) {
    //         const petya = new User( {username: 'Петя', password: '123'} );
    //         petya.save(function (err) {
    //             callback(err, petya);
    //         })
    //     },
    //     function (callback) {
    //         const admin = new User(  {username: 'admin', password: 'thetruehero'});
    //         admin.save(function (err) {
    //             callback(err, admin);
    //         })
    //     }
    // ], callback);
}
