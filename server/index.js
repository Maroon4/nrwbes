const express = require('express');
const http = require('http');
const path = require('path'); // NEW
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const session = require('express-session');
const errorHandler = require('express-error-handler');


const conf = require('./conf');
const HttpError = require('./error').HttpError;




const app = express();
const port = process.env.PORT || conf.get('port');
const DIST_DIR = path.join(__dirname, '../dist'); // NEW
const HTML_FILE = path.join(DIST_DIR, 'index.html'); // NEW
const mockResponse = {
    foo: 'bar',
    bar: 'foo'
};
//1

//
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

if (app.get('evn') === 'development') {
    app.use(logger('dev'));
} else {
    app.use(logger('default'));
}

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());

const MongoStore = require('connect-mongo')(session);

// app.use(session({
//     secret: conf.get('session:secret'),
//     key: conf.get('session:key'),
//     cookie: conf.get('session:cookie'),
//     // useUnifiedTopology: true,
//     store: new MongoStore({
//         mongooseConnection: mongoose.connection,
//         useUnifiedTopology: true
//     }),
//     proxy: true,
//     resave: true,
//     saveUninitialized: true
// }));


//1
app.use(express.static(DIST_DIR)); // NEW
app.get('/api', (req, res) => {
    res.send(mockResponse);
});
app.get('/', (req, res) => {
    res.sendFile(HTML_FILE); // EDIT
});

app.use(function (err, req, res, next) {

    if(typeof  err == 'number') {
        err = new HttpError(err);
    }

    if(err instanceof HttpError) {
        res.sendHttpError(err);
    } else {
        if (app.get('env') === 'development') {
            errorHandler()(err, req, res, next);
        } else {
            log.error(err);
            err = new HttpError(500);
            res.sendHttpError(err);
        }
    }


});




http.createServer(app).listen(conf.get('port'), function () {
    console.log('Express server listening on port' + conf.get('port'))
});


module.exports = app;