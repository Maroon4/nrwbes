const winston = require('winston');
const ENV = process.env.Node_ENV;

// const getLogger = winston.createLogger({
//     level: 'info',
//     format: winston.format.json(),
//     defaultMeta: { service: 'user-service' },
//     transports: [
//         //
//         // - Write all logs with level `error` and below to `error.log`
//         // - Write all logs with level `info` and below to `combined.log`
//         //
//         new winston.transports.File({ filename: 'error.log', level: 'error' }),
//         new winston.transports.File({ filename: 'combined.log' })
//     ]
// });
//
// if (process.env.NODE_ENV !== 'production') {
//     getLogger.add(new winston.transports.Console({
//         format: winston.format.simple()
//     }));
// }

function getLogger(module) {

    //TypeError: Cannot read property 'filename' of undefined
    const path = module.filename.split('/').slice(-2).join('/');

    return new winston.createLogger({
        transports: [
            new winston.transports.Console({
                colorize: true,
                level: ENV === 'development' ? 'debug' : 'error',
                label: path
            })
        ]
    });

}

module.exports = getLogger;
