const mongoose = require('mongoose');
const conf = require('../conf');


// mongoose.connect(conf.get(`mongoose:uri`), conf.get(`mongoose: options`), { useNewUrlParser: true });

const mongo = mongoose.connect(conf.get(`mongoose:uri`), { useNewUrlParser: true });

mongo.then(() => {
    console.log('connected');
}).catch((err) => {
    console.log('err', err);
});

module.exports = mongoose;
