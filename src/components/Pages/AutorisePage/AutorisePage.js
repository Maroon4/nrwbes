import React, {Component} from "react";
import {BrowserRouter as Router, Link, Redirect} from 'react-router-dom'


export default class AutorisePage extends Component{

    constructor(){
        super();

        this.state = {
            login: "",
            password: ""
        };

    }



    onSubmit = (event) =>{
        alert(`${this.state.login}, добро пожаловать!`);
        event.preventDefault();
    };

    onChangePassword = (event) => {
        this.setState({password: event.target.value});
    };

    onChangeLogin = (event) => {
        this.setState({login: event.target.value});
    };
    // goToRegistration = () => {
    //     return (
    //         <Redirect to="/registrationpage"/>
    //     );
    // };



    render() {

        return (
            <div>
                <div>
                    <h1>Авторизація</h1>
                    <form onSubmit={this.onSubmit}>
                        <p><label> Логин: <input type="text" name="login" value={this.state.login}
                                                 onChange={this.onChangeLogin}/></label></p>
                        <p><label> Пароль: <input type="password" name="password" value={this.state.password}
                                                  onChange={this.onChangePassword}/></label></p>
                        <p><input type="submit" value="Увійти" /></p>

                    </form>

                    <button
                        // onClick={this.goToRegistration}
                        >

                            Зареєструватися

                    </button>
                </div>
            </div>
        );
    }

}
