import React from 'react';
import ReactDOM from 'react-dom';

import App from "./components/App";
import './styles.scss'
// const Index = () => {
//     return <div>Welcome to React!</div>;
// };
ReactDOM.render(<App/>, document.getElementById('root'));